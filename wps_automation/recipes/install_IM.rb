#
#
#
#
#
#

#wps_db = data_bag_item('tracfone_wps_databag', "#{node.chef_environment}")
was_user = 'vagrant'
was_group = 'vagrant'


#im_package_path = wps_db['im_package_path']
#im_install_path = wps_db['im_install_path']

im_package_path = '/vagrant/Portal/SETUP/IIM/linux_x86_64'
im_install_path = '/opt/RC/'

execute "change_permisson" do
		command "chmod -R 755 #{im_package_path}"
	action :run
	not_if do FileTest.exist?("#{im_install_path}/IBMIM") end
end



execute "install_Installation_Manager " do

		#command "echo #{im_package_path}/installc -acceptLicense -silent -installationDirectory #{im_install_path}"
		command "#{im_package_path}/installc -acceptLicense -silent -installationDirectory #{im_install_path}"
	action :run
	not_if do FileTest.exist?("#{im_install_path}/IBMIM") end
	not_if do FileTest.directory?("/var/ibm/InstallationManager") end
end

#Creating a directory to contain all the trigger files
directory "/opt/CHEF" do
	owner "#{was_user}"
  group "#{was_group}"
	mode "00755"
end
