# Cookbook: tracfone_wps_st
# Recipe: create_dmgr_profile
#
# Author: Royal Cyber, Inc.
#
#



#wps_db = data_bag_item('tracfone_wps_databag', "#{node.chef_environment}")
#was_dmgr_install_path = node[:was][:was_dmgr_install_path]
#was_dmgr_install_path = wps_db['was_dmgr_install_path']
#was_dmgr_profile_name = node[:was][:was_dmgr_profile_name]
#was_dmgr_profile_name = wps_db['was_dmgr_profile_name']
#was_sit_node_name = node[:was][:was_sit_node_name]
#was_sit_node_name = wps_db['was_sit_node_name']
#was_sit_cell_name = wps_db['was_sit_cell_name']
#dmgr_user_password = wps_db['dmgr_user_password']
#dmgr_user = wps_db['dmgr_user']

was_user = 'vagrant'
was_group = 'vagrant'

im_install_path = '/opt/RC'
was_dmgr_install_path = '/opt/IBM/RCPortal'
was_dmgr_profile_name = 'Dmgr01'
was_cell_name = 'dmgrcell01'
was_node_name = 'dmgrnode01'
dmgr_user = 'wpsadmin'
dmgr_user_password = 'wpsadmin'


execute "Create dmgr profile " do
  command "#{was_dmgr_install_path}/bin/manageprofiles.sh -create -templatePath \
#{was_dmgr_install_path}/profileTemplates/management -hostName #{node[:fqdn]} \
-cellName #{was_cell_name} -nodeName #{was_node_name} -profileName #{was_dmgr_profile_name} \
-profilePath #{was_dmgr_install_path}/profiles/#{was_dmgr_profile_name} \
-enableAdminSecurity true -adminUserName #{dmgr_user} -adminPassword #{dmgr_user_password}"
  only_if do FileTest.directory?("#{was_dmgr_install_path}") end
  not_if do FileTest.directory?("#{was_dmgr_install_path}/profiles/#{was_dmgr_profile_name}") end
end
