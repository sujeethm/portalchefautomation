# Cookbook Name:: wps_automation
# Recipe:: install_was_dmgr
#
# Author: Royal Cyber
#
#

#wps_db = data_bag_item('tracfone_wps_databag', "#{node.chef_environment}")

#im_install_path = wps_db['im_install_path']
#was_repository_path = wps_db['was_repository_path']
#was_dmgr_install_path = wps_db['was_dmgr_install_path']

was_user = 'vagrant'
was_group = 'vagrant'

im_install_path = '/opt/RC'
was_repository_path = '/vagrant/Portal/WAS8552'
was_dmgr_install_path = '/opt/IBM/RCPortal'

template "/usr/was_dmgr.xml" do
    cookbook "wps_automation"
    source "was_install.xml.erb"
    #owner 'wasuser'
    #group 'wasgroup'
    mode "00755"
	variables(
		:installationID        =>	"IBM WebSphere Application Server V8.5",
		:was_repository_path   =>	"#{was_repository_path}",
		:was_install_path =>	"#{was_dmgr_install_path}"
	)
	not_if do FileTest.directory?("#{was_dmgr_install_path}") end
	only_if do FileTest.exist?("#{was_repository_path}/repository.config") end
    #notifies :restart, "service[dmgr]", :immediate
    action :create
end




execute "Install WAS for DMGR" do

		#command "echo #{im_package_path}/installc -acceptLicense -silent -installationDirectory #{im_install_path}"
		command "#{im_install_path}/eclipse/tools/imcl -acceptLicense -sP input /usr/was_dmgr.xml -log /tmp/install_was4dmgr.log"
	action :run
	only_if do FileTest.exist?("#{im_install_path}/eclipse/tools/imcl") end
	only_if do FileTest.exist?("#{was_repository_path}/repository.config") end
	not_if do FileTest.directory?("#{was_dmgr_install_path}") end

end
