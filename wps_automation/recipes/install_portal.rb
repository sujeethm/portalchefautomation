# Cookbook Name:: wps_automation
# Recipe:: install_portal
#
# Author: Royal Cyber
#
#

#wps_db = data_bag_item('tracfone_wps_databag', "#{node.chef_environment}")

was4portal_install_path = wps_db['was4portal_install_path']
portal_repository_path = wps_db['portal_repository_path']
portal_profile_parent_path = "#{was4portal_install_path}"[0...-10]
wp_hostname = node["fqdn"]

template "/usr/portal_install.xml" do
    cookbook "tracfone_wps_st"
    source "portal_install.xml.erb"
    #owner 'wasuser'
    #group 'wasgroup'
    mode "00755"

	variables(
		:portal_repository_path  =>	"#{portal_repository_path}",
		:was4portal_install_path =>	"#{was4portal_install_path}",
		:wp_hostname             =>	"#{wp_hostname}",
		:portal_profile_parent_path  =>  "#{portal_profile_parent_path}"
	)
	not_if do FileTest.directory?("#{portal_profile_parent_path}/PortalServer") end
	only_if do FileTest.directory?("#{portal_repository_path}") end
    action :create
end


execute "Install Portal Server" do

		command "#{im_install_path}/tools/imcl -acceptLicense -sP input /usr/portal_install.xml -log /tmp/install_portal.log"
	action :run
	only_if do FileTest.exist?("#{im_install_path}/tools/imcl") end
  only_if do FileTest.directory?("#{was4portal_install_path}") end
	not_if do FileTest.directory?("#{portal_profile_parent_path}/PortalServer") end
	only_if do FileTest.exist?("#{portal_repository_path}") end
end
